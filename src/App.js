import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';


import MainContent from './components/MainContent';
import HeaderBar from './components/HeaderBar';
import LeftBar from './components/LeftBar';

import styles from './MainStyles';

class App extends React.Component {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <HeaderBar
          open_left = {this.state.open}
          handleDrawerOpen = {this.handleDrawerOpen}
        />    
        <LeftBar
          open_left = {this.state.open}
          handleDrawerClose = {this.handleDrawerClose}
        />
        <MainContent/>
      </div>
    );
  }
}
export default withStyles(styles, { withTheme: true })(App);