import React from 'react'
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton';

import { withStyles } from '@material-ui/core/styles';


import styles from '../MainStyles';

class HeaderBar extends React.Component {
    render() {
        const { classes } = this.props;
        const { open_left } = this.props;
        const { handleDrawerOpen } = this.props;
        return (
            <AppBar
            position="fixed"
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open_left,
            })}
          >
            <Toolbar disableGutters={!open_left}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={handleDrawerOpen}
                className={classNames(classes.menuButton, {
                  [classes.hide]: open_left,
                })}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" noWrap>
                Mini variant drawer
              </Typography>
            </Toolbar>
          </AppBar>

        );
    }
};
export default withStyles(styles, { withTheme: true })(HeaderBar);